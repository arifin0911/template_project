import 'package:dependencies/app_dependencies.dart';
import 'package:flutter/foundation.dart';

class Crashlytics {
  static void initialize() {
    // Set up Flutter error handling
    FlutterError.onError = (FlutterErrorDetails errorDetails) {
      FirebaseCrashlytics.instance.recordFlutterFatalError(errorDetails);
    };

    // Set up platform dispatcher error handling
    PlatformDispatcher.instance.onError = (Object error, StackTrace stack) {
      FirebaseCrashlytics.instance.recordError(error, stack, fatal: true);
      return true;
    };
  }
}

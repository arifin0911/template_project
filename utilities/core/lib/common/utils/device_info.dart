import 'dart:io';

import 'package:dependencies/app_dependencies.dart';

@injectable
class DeviceInfo {
  var uuid = const Uuid();
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

  String getUuId() {
    return uuid.v1();
  }

  Future<String> getDeviceName() async {
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      final deviceModel = androidInfo.model;
      final deviceSDK = androidInfo.version.sdkInt.toString();
      final deviceBrand = androidInfo.manufacturer;
      final deviceOS = androidInfo.version.release;
      return 'Android $deviceOS (SDK $deviceSDK $deviceBrand $deviceModel)';
    }

    return '';
  }

  Future<String> getBuildNumber() async {
    final packageInfo = await PackageInfo.fromPlatform();
    return packageInfo.buildNumber;
  }

  Future<String> getVersion() async {
    final packageInfo = await PackageInfo.fromPlatform();
    return packageInfo.version;
  }

  Future<String> getDeviceInfo() async {
    final version = await getVersion();
    final buildNumber = await getBuildNumber();
    return '$version $buildNumber';
  }
}

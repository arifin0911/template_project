import 'package:dependencies/app_dependencies.dart';
import 'package:flutter/material.dart';

void openInternetSettings(BuildContext context) {
  if (Theme.of(context).platform == TargetPlatform.android) {
    const intent = AndroidIntent(
      action: 'android.settings.WIFI_SETTINGS',
    );
    intent.launch();
  } else if (Theme.of(context).platform == TargetPlatform.iOS) {
    launchUrl(Uri.parse('App-Prefs:root=WIFI'));
  }
}

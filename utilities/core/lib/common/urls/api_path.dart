class ApiPath {
  //still in example. adjust/change and remove this comment if the API endpoints are ready.
  static const superApps = '/superapps/mobile';
  static const login = '$superApps/auth/login';
  static const register = '/register';
  static const logout = '/myapproval/logout';

  static const approvalList = '/myapproval/get_list_outstanding';
  static const approvalDetail = '/myapproval/get_detail_md';
  static const approvalProcess = '/myapproval/md_actions';
  static const pushApprovalNotif = '/myapproval/send_notification';
  static const user = '/users';
  static const loadUserById = '';
  static const addUserById = '';
  static const updateUserById = '';
  static const deleteUserById = '';

  static const loadBMOrder = 'https://jsonkeeper.com/b/GUJR';
  static const loadBMOrderDetail = 'https://jsonkeeper.com/b/YZJ9';
  static const promo = '$superApps/banner';
  static const article = '$superApps/article';
  static const articleDetail = 'https://jsonkeeper.com/b/0GKZ';
  static const menu = 'https://jsonkeeper.com/b/7THT';

}

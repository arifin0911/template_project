import 'package:dependencies/app_dependencies.dart';

@module
abstract class ConnectivityDi {
  @lazySingleton
  Connectivity get connectivity => Connectivity();
}

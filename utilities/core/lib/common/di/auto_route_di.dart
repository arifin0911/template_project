import 'package:core/router/app_router.dart';
import 'package:dependencies/app_dependencies.dart';

@module
abstract class AutoRouteDi {
  @lazySingleton
  AppRouter get appRouter => AppRouter();
}

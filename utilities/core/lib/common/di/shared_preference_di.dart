import 'package:dependencies/app_dependencies.dart';

@module
abstract class SharedPreferencesDi {
  @preResolve
  Future<SharedPreferences> get sharedPreferences async =>
      SharedPreferences.getInstance();
}

import 'package:injectable/injectable.dart';

// final _remoteConfig = FirebaseRemoteConfigService();

abstract class Env {
  String get baseUrl;

  String get authKey;

  String get apiKey;
}

@Injectable(as: Env)
@dev
class DevEnv implements Env {
  @override
  String get baseUrl => 'http://192.168.17.178:8080';
  // _remoteConfig.baseUrlDev;

  @override
  String get apiKey => 'fiD4LFaUK4Sp9cggmPqTyN7ZUfEyUvQw';
  // _remoteConfig.apiKeyDev;

  @override
  String get authKey => 'superApps!8634';
  // _remoteConfig.authKeyDev;
}

@Injectable(as: Env)
@prod
class ProdEnv implements Env {
  @override
  String get baseUrl => '_remoteConfig.baseUrlProd';

  @override
  String get apiKey => '_remoteConfig.apiKeyProd';

  @override
  String get authKey => '_remoteConfig.authKeyProd';
}

@Injectable(as: Env)
@test
class TestEnv implements Env {
  @override
  String get baseUrl => '_remoteConfig.baseUrlTest';

  @override
  String get apiKey => '_remoteConfig.apiKeyTest';

  @override
  String get authKey => '_remoteConfig.authKeyTest';
}

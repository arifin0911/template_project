import 'package:core/config/injectable.config.dart';
import 'package:dependencies/app_dependencies.dart';

final getIt = GetIt.instance;

@InjectableInit(
  initializerName: 'init', // default
  preferRelativeImports: true, // default
  asExtension: false,
)
void configureDependencies(String env) => init(
      getIt,
      environment: env,
    );

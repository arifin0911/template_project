// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../env.dart' as _i3;

const String _dev = 'dev';
const String _prod = 'prod';
const String _test = 'test';

// initializes the registration of main-scope dependencies inside of GetIt
_i1.GetIt init(
  _i1.GetIt getIt, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) {
  final gh = _i2.GetItHelper(
    getIt,
    environment,
    environmentFilter,
  );
  gh.factory<_i3.Env>(
    () => _i3.DevEnv(),
    registerFor: {_dev},
  );
  gh.factory<_i3.Env>(
    () => _i3.ProdEnv(),
    registerFor: {_prod},
  );
  gh.factory<_i3.Env>(
    () => _i3.TestEnv(),
    registerFor: {_test},
  );
  return getIt;
}

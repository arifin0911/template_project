library core;

export 'package:core/config/injectable.dart';
export 'package:core/env.dart';
export 'package:core/gen/assets.gen.dart';
export 'package:core/router/app_router.dart';

/// A Calculator.
class Calculator {
  /// Returns [value] plus 1.
  int addOne(int value) => value + 1;
}
